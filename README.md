#Health Inspector Results from the City of Chicago - Front End

This simple project utilizes the data collected from the **City of Chicago Open Data** project, which offers up many different datasets to the public related to the great city.

The front end doesn't actually hit the Chicago SODA API that they have set up, but rather a Firebase DB that I've set up. I ETL the data that I find relevant, and then make it available via a Firebase DB (see the ```get_data.py``` file in the root directory for the script that I run weekly via a cronjob on my server to update my DB).

##Technologies Used

- React
- Material-UI (React-based Material Design Library)
- Foundation 6 (For their grid system)
- Webpack
- Radium (for styling React components)
- ES6 (For some features, mostly mixed in with ES5)
- Firebase - For accessing cleaned version of the data.
