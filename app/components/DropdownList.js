import React from 'react';
import Radium from 'radium';

// Components
import SelectField from 'material-ui/lib/select-field';

// Styles
import styles from '../styles/dropdown-list-styles';

class DropdownList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {value: null};
    // Add props styles if passed in
    this.style = $.extend({}, styles.root);
    if (this.props.style !== undefined) {
      this.style = $.extend(this.style, this.props.style);
    }
  }

  handleChange(event, index, value) {
    this.setState({value});
  }

  render() {
    return (
      <SelectField
        maxHeight={300}
        value={this.state.value}
        onChange={this.handleChange.bind(this)}
        style={ this.style }
        floatingLabelText={ this.props.label }
      >
        { this.props.items }
      </SelectField>
    );
  }
}

export default Radium(DropdownList);
