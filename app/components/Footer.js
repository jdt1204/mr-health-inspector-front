import React from 'react';
import Radium from 'radium';

// Styles
import styles from '../styles/footer-styles';

function Footer(props) {
  return (
    <div className="footer"  style={styles.root}>
      <div className="row">
        <div className="columns small-12 medium-12 large-12 text-center">
          <p>Come back soon to keep informed!</p>
        </div>
      </div>
    </div>
  )
}

export default Radium(Footer);
