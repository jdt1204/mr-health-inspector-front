import React from 'react';
import Radium from 'radium';

// Components
import Paper from 'material-ui/lib/paper';

// Styles
import styles from '../styles/form-container-styles';

var FormContainer = React.createClass({
  render: function() {
    return (
      <Paper zDepth={ this.props.zDepth } style={ styles.root }>
        { this.props.children }
      </Paper>
    )
  }
});

export default Radium(FormContainer);
