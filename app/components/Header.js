import React from 'react';
import Radium from 'radium';
import { Navigation } from 'react-router';

// Components
import AppBar from 'material-ui/lib/app-bar';

// Styles
import colors from '../styles/colors';

var Header = React.createClass({
  mixins: [Navigation],

  handleTitleTap: function() {
    window.location.hash = '/';
  },

  render: function() {
    return (
      <AppBar
        title={this.props.title}
        iconElementRight={ this.props.rightIcon }
        style={ {backgroundColor: colors.accentColor} }
        titleStyle={{ cursor: 'pointer' }}
        onTitleTouchTap={ this.handleTitleTap }
      />
    )
  }
});

export default Radium(Header);
