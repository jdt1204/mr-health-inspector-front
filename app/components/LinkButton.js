import React from 'react';
import Radium from 'radium';

// Components
import RaisedButton from 'material-ui/lib/raised-button';

// Styles
import styles from '../styles/link-button-styles';

function LinkButton(props) {
  // Set up the styles
  var style = $.extend({}, styles.root);
  style = $.extend(style, props.style);

  return (
    <RaisedButton
      label={ props.label }
      backgroundColor={ props.backgroundColor }
      className="material-button"
      disabled={ props.disabled }
      linkButton={ props.linkButton }
      href={ props.href }
      onTouchTap={ props.onTouchTap }
      keyboardFocused={ props.keyboardFocused }
      style={ style } />
  )
}

export default Radium(LinkButton);
