import React from 'react';
import Radium from 'radium';

// Components
import Dialog from 'material-ui/lib/dialog';
import LinkButton from './LinkButton';
import SearchForm from './SearchForm';

// Styles
import colors from '../styles/colors';
import styles from '../styles/modal-styles';

class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleOpen() {
    this.setState({open: true});
  };

  handleClose() {
    this.setState({open: false});
  };

  render() {
    const actions = [
      <LinkButton
        label="Ok"
        backgroundColor={ colors.accentColor }
        keyboardFocused={true}
        onTouchTap={this.handleClose}
        style={{ margin: '.5em' }}
      />,
    ];

    return (
      <div>
        <LinkButton
          label={ this.props.buttonText }
          style={ styles.mainButton }
          onTouchTap={ this.handleOpen }
          backgroundColor={ colors.mainColor } />
        <Dialog
          title={ this.props.dialogTitle }
          autoScrollBodyContent={ true }
          actions={ actions }
          modal={ false }
          open={ this.state.open }
          onRequestClose={ this.handleClose }
        >
          { this.props.children }
        </Dialog>
      </div>
    );
  }
}

export default Radium(Modal);
