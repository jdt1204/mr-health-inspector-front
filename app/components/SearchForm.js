import React from 'react';
import Radium from 'radium';

// Components
import MenuItem from 'material-ui/lib/menus/menu-item';
import DropdownList from './DropdownList';
import SubmitButton from './SubmitButton';
import TextInput from './TextInput';

// Styles
import styles from '../styles/search-form-styles';
import colors from '../styles/colors';

// Items for the dropdowns
import states from '../config/states';
import venueTypes from '../config/venueTypes';

// Prepare items for the dropdowns
var stateMenuItems = [];
for (var i = 0; i < states.states.length; i++) {
  stateMenuItems.push(<MenuItem value={states.states[i]} key={states.states[i]} primaryText={states.states[i]}/>);
}

var venueMenuItems = [];
for (i = 0; i < venueTypes.venueTypes.length; i++) {
  venueMenuItems.push(<MenuItem value={venueTypes.venueTypes[i]} key={venueTypes.venueTypes[i]} primaryText={venueTypes.venueTypes[i]}/>);
}

var risks = [];
for (i = 1; i < 4; i++) {
  risks.push(<MenuItem value={i} key={i} primaryText={i}/>);
}

var SearchForm = React.createClass({
  render: function() {
    return (
      <h2>Coming Soon!</h2>
    );
    return (
      <form className="search-form" style={ styles.root }>
        <div className="row">
          <div className="columns small-12 medium-6 large-6">
            <TextInput
              label="Venue Name"
            />
            <br />
            <TextInput
              label="Address" />
            <br />
            <TextInput
              label="City" />
            <br />
            <div className="row" style={{ marginTop: '0' }}>
              <div className="columns small-12 medium-6 large-6" style={{ textAlign: "left" }}>
                <DropdownList
                  items={ stateMenuItems }
                  label="State"
                  style={ styles.stateField } />
              </div>
              <div className="columns small-12 medium-6 large-6">
                <TextInput
                  label="ZIP"
                  style={ styles.zipField } />
              </div>
            </div>
          </div>
          <div className="columns small-12 medium-6 large-6">
            <DropdownList
              items={ venueMenuItems }
              label="Venue Type" />
            <br />
            <DropdownList
              items={ risks }
              label="Risk" />
            <br />
            <DropdownList
              items={ stateMenuItems }
              label="Inspection Type" />
          </div>
        </div>
        {/*<div className="row">
          <div className="columns" style={ styles.buttonContainer }>
            <SubmitButton
              backgroundColor={ colors.accentColor }
              label="Submit" />
          </div>
        </div>*/}
      </form>
    )
  }
});

export default Radium(SearchForm);
