import React from 'react';
import Radium from 'radium';

// Styles
import styles from '../styles/section-styles';

function Section(props) {
  // Get color for background of div
  var style = $.extend({}, styles.root);
  if (props.color !== undefined) {
    style.backgroundColor = props.color;
  }

  return (
    <div className="section" style={ style }>
      { props.children }
    </div>
  )
}

export default Radium(Section);
