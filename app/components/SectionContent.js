import React from 'react';
import Radium from 'radium';

// Styles
import styles from '../styles/section-content-styles';

function SectionContent(props) {
  // Determine text color
  var style = $.extend({}, styles.root);
  if (props.color !== undefined) {
    style.color = props.color;
  }


  return (
    <div className="section-content" style={ style }>
      { props.children }
    </div>
  )
}

export default Radium(SectionContent);
