import React from 'react';
import Radium from 'radium';

// Styles
import styles from '../styles/section-header-styles';

function SectionHeader(props) {
  // Determine text color
  var style = $.extend({}, styles.h2);
  if (props.color !== undefined) {
    style.color = props.color;
  }

  if (props.style !== undefined) {
    style = $.extend(style, props.style);
  }

  return (
    <div className="section-header" style={ styles.root }>
      <h2 style={ style }>
        {props.children}
      </h2>
    </div>
  )
}

export default Radium(SectionHeader);
