import React from 'react';
import Radium from 'radium';

// Components
import RaisedButton from 'material-ui/lib/raised-button';

// Styles
import styles from '../styles/submit-button-styles';

function SubmitButton(props) {
  return (
    <RaisedButton
      label={ props.label }
      backgroundColor={ props.backgroundColor }
      className="material-button"
      disabled={ props.disabled }
      linkButton={ false } />
  )
}

export default Radium(SubmitButton);
