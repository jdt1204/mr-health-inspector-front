import React from 'react';
import Radium from 'radium';

// Components
import TextField from 'material-ui/lib/text-field';

// Styles
import styles from '../styles/text-input-styles';

var TextInput = React.createClass({
  render: function() {
    // Add props styles if passed in
    var style = $.extend({}, styles.root);
    if (this.props.style !== undefined) {
      style = $.extend(style, this.props.style);
    }

    return (
      <TextField
        floatingLabelText={ this.props.label }
        style={ style } />
    )
  }
});

export default Radium(TextInput);
