import React from 'react';
import Radium from 'radium';

// Components
import Modal from './Modal';

// Styles
import colors from '../styles/colors';
import styles from '../styles/venue-information-styles';

function VenueInformation(props) {
  // Determine results font size:
  var resultsBoxStyle = $.extend({}, styles.resultsBox);
  if (props.selectedItem.name === '') {
    return (
      <div>
        <h2>Choose a place from the list on the left!</h2>
        <img src="http://i.imgur.com/DCUuz3K.png" />
      </div>
    )
  } else {
    if (props.selectedItem.results.indexOf('Condition') !== -1) {
      resultsBoxStyle.backgroundColor = colors.yellow;
      resultsBoxStyle.fontSize = '2em';
    } else if (props.selectedItem.results.indexOf('Pass') !== -1) {
      resultsBoxStyle.backgroundColor = colors.green;
    } else if (props.selectedItem.results.indexOf('Fail') !== -1) {
      resultsBoxStyle.backgroundColor = colors.red;
    } else if (props.selectedItem.results.indexOf('Out') !== -1) {
      resultsBoxStyle.backgroundColor = colors.red;
      resultsBoxStyle.fontSize = '2em';
    } else if (props.selectedItem.results.indexOf('Not') !== -1) {
      resultsBoxStyle.backgroundColor = colors.yellow;
    }

    var date = new Date(props.selectedItem.inspectionDate);
    var monthNames = [
      "January", "February", "March",
      "April", "May", "June", "July",
      "August", "September", "October",
      "November", "December"
    ];

    var formattedDate = monthNames[date.getMonth()] + " " + date.getDate() + ", " + date.getFullYear();

    if (props.selectedItem.violations !== undefined && props.selectedItem.violations.length > 0) {
      var formattedViolations = props.selectedItem.violations.map(function(row, index) {
        return (
          <p key={ index }>{ row }</p>
        )
      })

    } else {
      var formattedViolations = (
        <div>
          <h5>These aren't the violations that you're looking for...</h5>
          <p>(... Meaning they didn't have any violations during this inspection!)</p>
        </div>
      )
    }

    return (
      <div style={ styles.root }>
        <div style={ resultsBoxStyle }>{ props.selectedItem.results }</div>

        <h2  style={ styles.h2 }>{ props.selectedItem.name }</h2>
        <div style={ styles.address }>
          { props.selectedItem.address }<br />
          { props.selectedItem.city }, {props.selectedItem.state} {props.selectedItem.zip}
        </div>

        <div style={ styles.date }>Last Inspected: { formattedDate }</div>

        <div style={ styles.risk }>{ props.selectedItem.risk }</div>

        <div style={ styles.modal }>
          <Modal
            buttonText="View Violations"
            dialogTitle="Violations" >
            { formattedViolations }
          </Modal>
        </div>

      </div>
    )
  }
}

export default Radium(VenueInformation);
