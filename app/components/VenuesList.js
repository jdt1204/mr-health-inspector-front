import React from 'react';
import Radium from 'radium';
import ReactFireMixin from 'reactfire'

// Components
import Paper from 'material-ui/lib/paper';
import List from 'material-ui/lib/lists/list';
import VenueInformation from './VenueInformation';
import LinkButton from '../components/LinkButton';
import ListItem from 'material-ui/lib/lists/list-item';
import CircularProgress from 'material-ui/lib/circular-progress';
import {SelectableContainerEnhance} from 'material-ui/lib/hoc/selectable-enhance';

// Styles
import colors from '../styles/colors';
import styles from '../styles/venues-list-styles';

let FIREBASE_URL   = "https://mr-health-inspector.firebaseIO.com/venues"
let SelectableList = SelectableContainerEnhance(List);

function wrapState(Component) {
  const VenuesList = React.createClass({
    mixins: [ReactFireMixin],

    getInitialState: function() {
      return {
        index: 0,
        selectedIndex: -1,
        venues: [],
        paginationRate: 25,
        selectedItem: {
          name: '',
          address: '',
          city: '',
          state: '',
          zip: '',
          results: '',
          type: '',
          risk: '',
          inspectionDate: '',
          violations: ''
        },
        prevButtonDisabled: true
      }
    },

    componentDidMount: function() {
      this.fetchData();
    },

    fetchData: function() {
      var ref = new Firebase(FIREBASE_URL);
      this.bindAsArray(ref.startAt(this.state.index).endAt(this.state.index + this.state.paginationRate).orderByChild("id"), "venues");
    },

    handleUpdateSelectedIndex(e, index) {

      var newSelectedObject = {
        name          : this.state.venues[index].dba_name,
        address       : this.state.venues[index].address,
        city          : this.state.venues[index].city,
        state         : this.state.venues[index].state,
        zip           : this.state.venues[index].zip,
        results       : this.state.venues[index].results,
        type          : this.state.venues[index].type,
        risk          : this.state.venues[index].risk,
        inspectionDate: this.state.venues[index].inspection_date,
        violations    : this.state.venues[index].violations
      };

      this.setState({
        selectedIndex: index,
        selectedItem: newSelectedObject
      });
    },

    handlePaginationRateChange: function(newRate) {
      this.setState({ paginationRate: newRate });
    },

    fetchPrevious: function() {
      if (this.state.index > 0) {
        var newIndex = this.state.index - this.state.paginationRate;

        this.unbind("venues");
        this.setState({
          index: newIndex,
          selectedIndex: -1,
          venues: [],
          selectedItem: {
            name: '',
            address: '',
            city: '',
            state: '',
            zip: '',
            results: '',
            type: '',
            risk: '',
            inspectionDate: '',
            violations: ''
          }
        }, function() {
          if (this.state.index === 0) {
            this.setState({
              prevButtonDisabled: true
            });
          }
          this.fetchData();
        });
      }
    },

    fetchNext: function() {
      var newIndex = this.state.index + this.state.paginationRate;

      this.unbind("venues");
      this.setState({
        index: newIndex,
        venues: [],
        selectedIndex: -1,
        selectedItem: {
          name: '',
          address: '',
          city: '',
          state: '',
          zip: '',
          results: '',
          type: '',
          risk: '',
          inspectionDate: '',
          violations: ''
        },
        prevButtonDisabled: false
      }, function() {
        this.fetchData();
      });
    },

    render: function() {
      // Set up the rows
      if (this.state.venues.length !== 0) {
        var data = this.state.venues.map(function(row, index) {
          return (
            <ListItem
              key={ index }
              style={ styles.listItem }
              value={ index }
              primaryText={ row.dba_name + ' - ' + row.results }
              secondaryText={ row.address + " " + row.city + ", " + row.state } />
          );
        });
        return (
          <div style={ styles.root }>

            {/*Pagination*/}
            <div style={ styles.paginationContainer }>
              <LinkButton
                label="Previous"
                backgroundColor={ colors.mainColor }
                onTouchTap={ this.fetchPrevious }
                disabled={ this.state.prevButtonDisabled }/>
              <LinkButton
                label="Next"
                backgroundColor={ colors.mainColor }
                onTouchTap={ this.fetchNext } />
            </div>

            <div style={ styles.listInfoContainer }>
              {/*List Container*/}
              <Paper style={ styles.listContainer }>
                <Component
                  {...this.props}
                  {...this.state}
                  valueLink={{value: this.state.selectedIndex, requestChange: this.handleUpdateSelectedIndex}}>
                    { data }
                </Component>
              </Paper>

              {/*Venue Information*/}
              <div style={ styles.informationContainer }>
                <Paper style={ styles.paperInfo }>
                  <VenueInformation
                    selectedItem={this.state.selectedItem} />
                </Paper>
              </div>
            </div>

          </div>
        );
      } else {
        return (
          <CircularProgress />
        )
      }
    }
  });
  return VenuesList;
}

SelectableList = wrapState(SelectableList);

export default Radium(SelectableList);
