import React from 'react';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';

import Main from '../pages/Main';
import Home from '../pages/Home';
import UseIt from '../pages/UseIt';

var routes = (
  <Router onUpdate={() => window.scrollTo(0, 0)} history={ hashHistory }>
    <Route path='/' component={ Main }>
      <IndexRoute component={ Home } />
      <Route path="/UseIt" component={ UseIt } />
    </Route>
  </Router>
);

module.exports = routes;
