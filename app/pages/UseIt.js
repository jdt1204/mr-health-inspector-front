import React from 'react';
import ReactDOM from 'react-dom';
import Radium from 'radium';

// Components
import Header from '../components/Header';
import Dialog from 'material-ui/lib/dialog';
import Section from '../components/Section';
import LinkButton from '../components/LinkButton';
import SearchForm from '../components/SearchForm';
import IconButton from 'material-ui/lib/icon-button';
import SectionHeader from '../components/SectionHeader';
import SectionContent from '../components/SectionContent';
import Search from 'material-ui/lib/svg-icons/action/search';
import VenuesList from '../components/VenuesList';

// Styles
import colors from '../styles/colors';
import styles from '../styles/useit-styles';

var UseIt = React.createClass({
  getInitialState: function() {
    return {
      open: false
    }
  },

  handleOpen: function() {
    this.setState({open: true});
  },

  handleClose: function() {
    this.setState({open: false});
  },

  render: function() {
    const actions = [
      <LinkButton
        label="Cancel"
        onTouchTap={this.handleClose}
        style={{ margin: '.5em' }}
      />,
      <LinkButton
        label="Submit"
        backgroundColor={ colors.accentColor }
        keyboardFocused={true}
        onTouchTap={this.handleClose}
        style={{ margin: '.5em' }}
      />,
    ];

    return (
      <div className="use-it-content" style={ styles.root }>
        <Header
          title="Mr. Health Inspector - Chicago"
          rightIcon= {
            <IconButton onClick={ this.handleOpen }>
              <Search />
            </IconButton>
          }
        />

        <Section style={ styles.resultsTableSection }>
          <SectionHeader style={{ marginBottom: '0' }}>
            Venues
          </SectionHeader>

            {/*Search Filters*/}
            <Dialog
              title="Search"
              actions={actions}
              modal={false}
              open={this.state.open}
              onRequestClose={this.handleClose}
            >
              <SearchForm />
            </Dialog>

            {/*List of Venues*/}
            <VenuesList />
        </Section>
      </div>
    )
  }
});

export default Radium(UseIt);
