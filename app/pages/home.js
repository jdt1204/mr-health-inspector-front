import React from 'react';
import Radium from 'radium';
import { Link } from 'react-router';

// Styles
import styles from '../styles/home-styles';
import colors from '../styles/colors';

// Components
import Section from '../components/Section';
import SectionHeader from '../components/SectionHeader';
import SectionContent from '../components/SectionContent';
import LinkButton from '../components/LinkButton';
import Header from '../components/Header';
import IconButton from 'material-ui/lib/icon-button';
import IconMenu from 'material-ui/lib/menus/icon-menu';
import MoreVertIcon from 'material-ui/lib/svg-icons/navigation/more-vert';
import MenuItem from 'material-ui/lib/menus/menu-item';

var Home = React.createClass({
  render: function() {
    return (
      <div className="home-content" style={ styles.root }>
        <Header
          title="Mr. Health Inspector - Chicago"
          rightIcon={
            <IconMenu
              iconButtonElement={
                <IconButton><MoreVertIcon /></IconButton>
              }
              targetOrigin={{horizontal: 'right', vertical: 'top'}}
              anchorOrigin={{horizontal: 'right', vertical: 'top'}}
            >
              <MenuItem primaryText="More Settings Coming Soon" />
            </IconMenu>
          }
        />
        <Section color={ colors.white }>
          <SectionHeader>What Is This?</SectionHeader>
          <SectionContent>
            <p>It's a tool to allow you to see how your favorite restaurants are doing.</p>
            <p>Want to try a new place out? Got a recommendation from a friend? Make sure it's clean and safe before putting their food into your body!</p>
          </SectionContent>
        </Section>
        <Section color={ colors.mainColor }>
          <SectionHeader color={ colors.white }>Where Does the Data Come From?</SectionHeader>
          <SectionContent color={ colors.white }>
            <p>Right now we're just pulling data from the Chicago Open Data Project. Come back later for more cities!</p>
            <LinkButton
              label="Check Out Source"
              backgroundColor={ colors.accentColor }
              disabled={ false }
              linkButton={ true }
              href="https://data.cityofchicago.org/" />
          </SectionContent>
        </Section>
        <Section color={ colors.white }>
          <SectionHeader>How Do I Use This?</SectionHeader>
          <SectionContent>
            <p>Head on over to the "Use It" page to get started!</p>
            <Link to="/UseIt">
              <LinkButton
                label="Use It"
                backgroundColor={ colors.accentColor }
                disabled={ false }
                href={ undefined }
                linkButton={ false } />
            </Link>
          </SectionContent>
        </Section>
      </div>
    )
  }
});

export default Radium(Home);
