import React from 'react';
import { Style } from 'radium';

// Custom/Libary modules
import AppBar from 'material-ui/lib/app-bar';
import Footer from '../components/Footer';

// App-level styles
import styles from '../styles/app-styles';

function Main(props) {
  return (
    <div className="main-container">
      <Style rules={ styles } />
      { props.children }
      <Footer />
    </div>
  );
}

export default Main;
