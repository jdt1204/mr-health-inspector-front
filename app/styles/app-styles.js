import colors from './colors';

export default {
  body: {
    fontFamily: 'Roboto, sans-serif',
    backgroundColor: colors.backgroundDark,
    lineHeight: 'normal'
  },
  h1: {
    fontFamily: 'Roboto, sans-serif'
  },
  h2: {
    fontFamily: 'Roboto, sans-serif'
  },
  h3:{
    fontFamily: 'Roboto, sans-serif'
  },
  p: {
    fontFamily: 'Roboto, sans-serif'
  },
  ".row": {
    marginTop: '1.5em',
    marginBottom: '.5em'
  },
  "[type=text]": {
    boxShadow: 'none'
  },
  "[type=text]:focus": {
    boxShadow: 'none'
  },
  ".wrapper": {
    maxWidth: "900px"
  }
}
