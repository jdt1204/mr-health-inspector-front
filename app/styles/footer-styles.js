import colors from './colors';

export default {
  root: {
    backgroundColor: colors.backgroundDark,
    color: colors.white,
    height: '300px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: "center"
  }
}
