import colors from './colors';

export default {
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: '2em',
    width: '100%'
  }
}
