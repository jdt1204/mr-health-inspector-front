import colors from './colors';

export default {
  root: {
    lineHeight: 'normal',
    margin: '1em'
  }
}
