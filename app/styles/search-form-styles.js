import colors from './colors';

export default {
  root: {

  },
  buttonContainer: {
    textAlign: 'center'
  },
  zipField: {
    width: '40%',
    '@media screen and (maxWidth: 800px)': {
      width: '100%'
    }
  },
  stateField: {
    width: '200px',
    '@media screen and (minWidth: 800px)': {
    }
  }
}
