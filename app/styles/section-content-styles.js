import colors from './colors';

export default {
  root: {
    color: colors.black,
    fontSize: "1.5em",
    maxWidth: "900px",
    textAlign: "center"
  }
}
