import colors from "./colors";

export default {
  root: {
    marginTop: '1em',
    marginBottom: "0",
    textAlign: "center"
  },
  h2: {
    fontSize: "4em",
    color: colors.black
  }
}
