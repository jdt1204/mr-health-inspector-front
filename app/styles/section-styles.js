import colors from './colors';

export default {
  root: {
    backgroundColor: colors.white,
    minHeight: '35em',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  }
}
