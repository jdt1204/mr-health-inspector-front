import colors from './colors';

export default {
  root: {

  },
  h2: {
    marginLeft: 0
  },
  resultsBox: {
    float: 'right',
    fontSize: '3.25em',
    color: 'white',
    textShadow: '2px 2px 0px black',
    borderRadius: '5px',
    boxShadow: '0px 10px 10px rgb(129, 129, 128)',
    padding: '.5em',
    width: '200px',
    textAlign: 'center'
  },
  address: {
    fontSize: '1em',
    color: 'rgb(154, 154, 154)'
  },
  date: {
    margin: '1em 0',
    fontSize: '1em',
    fontStyle: 'italic'
  },
  risk: {
    margin: '1em 0',
    fontSize: '1.5em'
  },
  modal: {
    textAlign: 'center'
  }
}
