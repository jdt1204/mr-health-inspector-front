import colors from './colors';

export default {
  root: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  listInfoContainer: {
    display: 'flex',
    flexDirection: 'row'
  },
  listContainer: {
    flex: '.45',
    padding: '1em',
    maxHeight: '700px',
    overflow: 'scroll'
  },
  paginationContainer: {
    textAlign: 'center',
    margin: '1em 0'
  },
  informationContainer: {
    flex: '.55',
    padding: '0 1em'
  },
  listItem: {
    textAlign: 'left'
  },
  paperInfo: {
    padding: '1em'
  }
}
