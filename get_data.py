#!/usr/bin/python3
import hashlib, string
from sodapy import Socrata
from firebase import firebase

# Firebase DB URL
FIREBASE_URL = 'https://mr-health-inspector.firebaseIO.com'


def calcHash(name, facility_type, state):
    toHash = "{}{}{}".format(name, facility_type, state)
    return hashlib.sha256(toHash.encode())


# Create client
client = Socrata("data.cityofchicago.org", None)

######################
###### Query Data
# Exclude records with values that might be missing.
# Values to check for missing data:
#   - dba_name
#   - address
#   - city
#   - state
#   - zip
#   - risk
#   - results
#   - facility type
#   - inspection date
#   - latitude
#   - longitude
columns      = ['dba_name', 'address', 'city', 'state', 'zip', 'risk', 'results', 'facility_type', 'inspection_date', 'latitude', 'longitude']
where_clause = ""
for col in columns:
    where_clause += "{} IS NOT NULL AND ".format(col)

# Clean last "AND "
where_clause = where_clause[:-4]

# Set up pagination rate
#   As of 3/30/2016, the default pagination limit for SODA APIs is 1000, so we'll need to provide
#   a value for the limit. The current max is 50,000, and the current average inspections / year
#   is about 2,000. So, we'll just request 50,000, ordering by inspection_date, and call that good.
rawData = client.get('cwig-ma7x', where=where_clause, limit=50000, order="inspection_date DESC")

######################
###### Clean the data
# Due to the current nature of the app, we now can delete out any duplicate records, keeping only
# the most recent inspection, since V1 of this app only shows the current state of inspection.
# In future versions, this can be deleted when we add in support for seeing historical venue
# inspections.
#
# A record is considered duplicate if it contains the same values for the following columns:
#   - dba_name
#   - facility_type
#   - state
# This check can (and probably should) be augmented in the future.
# In order to check this as efficiently as possible, we'll create a hash out of the values for
# those attributes and store it with the object in our cleanedData. We'll then compare new hashes
# with hashes of cleaned data stored in a list
#
# The overall object to be stored to the FirebaseDB will look something like this:
# {
#   'id'             : incrementing counter
#   'hash'           : asldkfjasdlkfjaslkdfjasldfj,
#   'dba_name'       : dba_name,
#   'address'        : address,
#   'city'           : city,
#   'state'          : state,
#   'zip'            : zip,
#   'risk'           : risk,
#   'results'        : results,
#   'facility_type'  : facility_type,
#   'inspection_date': inspection_date,
#   'latitude'       : latitude,
#   'longitude'      : longitude,
#   'violations'     : violations
# }
cleanedData   = []
cleanedHashes = []
count         = 0

for val in rawData:
    valHash = calcHash(val['dba_name'], val['facility_type'], val['state'])
    if valHash.hexdigest() not in cleanedHashes:
        cleanedHashes.append(valHash.hexdigest())

        # Clean violations
        try:
            violations = [ string.capwords(v.strip()) for v in val['violations'].split("|") ]
        except KeyError:
            violations = []

        # Clean Date
        date = val['inspection_date']

        newVenue = {
            'id'             : count,
            'hash'           : valHash.hexdigest(),
            'dba_name'       : string.capwords(val['dba_name']),
            'address'        : val['address'],
            'city'           : string.capwords(val['city']),
            'state'          : val['state'].upper(),
            'zip'            : val['zip'],
            'risk'           : val['risk'],
            'results'        : val['results'],
            'facility_type'  : string.capwords(val['facility_type']),
            'inspection_date': val['inspection_date'],
            'latitude'       : val['latitude'],
            'longitude'      : val['longitude'],
            'violations'     : violations,
        }

        cleanedData.append(newVenue)
        count += 1

######################
###### Push the data
firebaseClient = firebase.FirebaseApplication(FIREBASE_URL, None)

# Clear out previous data in the venues node
firebaseClient.delete('/venues', None)

# Push each individual node up to the /venues node
for val in cleanedData:
    result = firebaseClient.post('/venues', val, params={'print': 'pretty'}, headers={ 'X_FANCY_HEADER': 'VERY FANCY' })

# Close SODA client
client.close()

print('###############################')
print('Successfully updated data in DB.')
print('###############################')
